<?php
require_once('config.php');

class Db
{
    private static $instance = null;
    protected $connection;

    private function __construct()
    {
        global $settings;
        $this->connection = new mysqli(
            "{$settings['host']}:{$settings['port']}",
            $settings['username'],
            $settings['password'],
            $settings['db']
        );
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
    }

    public static function getInstance(): Db
    {
        if (self::$instance == null) {
            self::$instance = new Db();
        }
        return self::$instance;
    }

    public function create(string $table, array $data): void
    {
        $this->connection->query(
            "INSERT INTO {$table} (" . implode(
                ',',
                array_keys($data)
            ) . ") VALUES (" . implode(',', array_values($data)) . ")"
        );
    }

    public function update(string $table, array $data, string $where): void
    {
        $fields = implode(
            ',',
            array_map(
                function ($key) use ($data) {
                    return $key . '=' . $data[$key];
                },
                array_keys($data)
            )
        );
        $sql = "update {$table} set {$fields} where {$where}";
        $this->connection->query($sql);
    }

    public function select(string $query)
    {
        return $this->connection->query($query);
    }

    public function escape_string($string)
    {
        return "'{$this->connection->escape_string($string)}'";
    }
}

class EventManager
{
    public static function handleEvent()
    {
        $db = Db::getInstance();
        $data = [
            'ip_address' => $db->escape_string(self::getIp()),
            'user_agent' => $db->escape_string($_SERVER['HTTP_USER_AGENT']),
            'view_date' => 'utc_timestamp()',
            'page_url' => $db->escape_string($_SERVER['HTTP_REFERER']),
            'views_count' => 1,
        ];
        $result = $db->select("SELECT id, views_count 
                            from banner 
                            where ip_address={$data['ip_address']}
                  and user_agent={$data['user_agent']}
                  and page_url={$data['page_url']}");
        if ($result->num_rows > 0) {
            $row = $result->fetch_row();
            $data['views_count'] = (int)$row[1]+1;
            $id = $row[0];
            $where = "id={$id}";
            $db->update('banner', $data, $where);
        } else {
            $db->create('banner', $data);
        }
    }

    private static function getIp(): string
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}

class Banner
{
    public static function display(): void
    {
        EventManager::handleEvent();
        self::displayImage();
    }

    private static function displayImage(): void
    {
        $name = './img/placeholder.png';
        header("Content-Type: image/png");
        header("Content-Length: " . filesize($name));
        echo file_get_contents($name);
        exit;
    }
}

$banner = new Banner();
$banner->display();
echo "Connected successfully";
